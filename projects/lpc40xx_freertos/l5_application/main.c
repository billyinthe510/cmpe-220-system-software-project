#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "l3_drivers\gpio.h"
#include "semphr.h"
#include "ssp2.h"
#include <stdio.h>

SemaphoreHandle_t spi_bus_mutex;

typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
} adesto_flash_id_s;

static void power_off_leds(void);
static void adesto_cs(void);
static void adesto_ds(void);
adesto_flash_id_s adesto_read_signature(void);
static void adesto_cs_unop(void);
static void adesto_ds_unop(void);
adesto_flash_id_s adesto_read_signature_unop(void);
// void spi_task(void *p);
void spi_id_verification_task(void *p);
void spi_id_verification_task_unop(void *p);

void main(void) {
  power_off_leds();
  spi_bus_mutex = xSemaphoreCreateMutex();

  // Create two tasks that will continously read signature
  // xTaskCreate(spi_id_verification_task, "Spi Verification Task 1", 2048 / (sizeof(void *)), NULL, PRIORITY_LOW,
  // NULL);
  xTaskCreate(spi_id_verification_task_unop, "Spi Verification Task 2", 2048 / (sizeof(void *)), NULL, PRIORITY_LOW,
              NULL);

  vTaskStartScheduler();
}

void power_off_leds(void) {
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
}

void spi_id_verification_task(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1)) {
      const adesto_flash_id_s id = adesto_read_signature();

      // When we read a manufacturer ID we do not expect, we will kill this task
      xSemaphoreGive(spi_bus_mutex);
    }
  }
}

adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s data = {0};
  TickType_t start_tick = xTaskGetTickCount();
  const uint32_t spi_clock_mhz = 24;
  ssp2__initialize(spi_clock_mhz);

  adesto_cs();

  static const uint8_t man_id_opcode = 0x9F;
  static const uint8_t dummy_value = 0xFF;
  uint8_t recieved_data = ssp2__exchange_byte(man_id_opcode);

  data.manufacturer_id = ssp2__exchange_byte(dummy_value);
  data.device_id_1 = ssp2__exchange_byte(dummy_value);
  data.device_id_2 = ssp2__exchange_byte(dummy_value);

  adesto_ds();
  printf("Optimized Start Tick Time is %lu \n", start_tick);
  printf("Optimized Stop Tick Time is %lu \n", xTaskGetTickCount());
  printf("Optimized Stop SPI Tick Time is %lu \n", (xTaskGetTickCount() - start_tick));
  return data;
}

void adesto_cs(void) {
  const uint32_t external_flash_cs_pin = 10;
  gpio_s ssp_gpio_cs_pin = gpio__construct_as_output(GPIO__PORT_1, external_flash_cs_pin);
  gpio__reset(ssp_gpio_cs_pin);
}
void adesto_ds(void) {
  const uint32_t external_flash_cs_pin = 10;
  gpio_s ssp_gpio_cs_pin = gpio__construct_as_output(GPIO__PORT_1, external_flash_cs_pin);
  gpio__set(ssp_gpio_cs_pin);
}

void spi_id_verification_task_unop(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1)) {
      const adesto_flash_id_s id = adesto_read_signature_unop();

      // When we read a manufacturer ID we do not expect, we will kill this task
      xSemaphoreGive(spi_bus_mutex);
    }
  }
}

adesto_flash_id_s adesto_read_signature_unop(void) {
  adesto_flash_id_s data = {0};
  TickType_t start_tick = xTaskGetTickCount();
  const uint32_t spi_clock_mhz = 24;
  ssp2__initialize_unop(spi_clock_mhz);
  adesto_cs_unop();

  static const uint8_t man_id_opcode = 0x9F;
  static const uint8_t dummy_value = 0xFF;
  uint8_t recieved_data = ssp2__exchange_byte(man_id_opcode);

  data.manufacturer_id = ssp2__exchange_byte(dummy_value);
  data.device_id_1 = ssp2__exchange_byte(dummy_value);
  data.device_id_2 = ssp2__exchange_byte(dummy_value);

  adesto_ds_unop();
  printf("UnOptimized Start Tick Time is %lu \n", start_tick);
  printf("UnOptimized Stop Tick Time is %lu \n", xTaskGetTickCount());
  printf("UnOptimized Stop SPI Tick Time is %lu \n", (xTaskGetTickCount() - start_tick));
  return data;
}

void adesto_cs_unop(void) {
  const uint32_t external_flash_cs_pin = 10;
  gpio_s ssp_gpio_cs_pin = gpio__construct_as_output(GPIO__PORT_1, external_flash_cs_pin);
  gpio__reset(ssp_gpio_cs_pin);
}
void adesto_ds_unop(void) {
  const uint32_t external_flash_cs_pin = 10;
  gpio_s ssp_gpio_cs_pin = gpio__construct_as_output(GPIO__PORT_1, external_flash_cs_pin);
  gpio__set(ssp_gpio_cs_pin);
}